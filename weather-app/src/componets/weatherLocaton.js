import React from 'react';
import Loction from './Location'
import WeatherData from './WeatherData';

const weatherLocation = () => (
    <div>
        <Loction></Loction>
        <WeatherData></WeatherData>
    </div>
);

export default weatherLocation;